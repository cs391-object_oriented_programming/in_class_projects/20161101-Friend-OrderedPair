#include<iostream>
#include<vector>

using namespace std;

class OrdPair {
	public:
		OrdPair( ) {  //default constructor to initialize values
			p1 = 0.0;
			p2 = 0.0;
		}
		OrdPair( float f1 , float f2 ) {  //constructor
			p1 = f1;
			p2 = f2;
		}
		bool operator==(const OrdPair& ) const;  //prototypes
		friend OrdPair operator*( const OrdPair&, const OrdPair& );
		friend OrdPair operator-(const OrdPair&, const OrdPair& );
		friend OrdPair operator+( const OrdPair&, const OrdPair&);
		void write_it( ) const;

	private:
		float p1, p2;
};

bool OrdPair::operator==(const OrdPair& s) const {
	return p1 == s.p1 && p2 == s.p2;
}

OrdPair operator*( const OrdPair& s, const OrdPair& q) { //use const to avoid altering member values
	OrdPair z(s.p1*q.p1, s.p2*q.p2);
	return z;
}

OrdPair operator-( const OrdPair& s, const OrdPair& q) {
	OrdPair z(s.p1 - q.p1, s.p2 - q.p2);
	return z;
}
OrdPair operator+( const OrdPair& s, const OrdPair& q) {
	OrdPair z(s.p1 + q.p1, s.p2 + q.p2);
	return z;
}

void OrdPair::write_it( ) const {
	cout << "The result is: (" << p1 << "," << p2 << ")" << endl;
}

int main() {
	OrdPair s1(32, 2), s2(32,-2), s3;
	if(s1.operator==(s2))
		// or can use
		//if (s1 == s2)
		cout << "equal" << endl;
	else
		cout<< "Not equal" << endl;


//	printf("\n  Original values of pair: \n\t\ts1=(%d,%d) | s2=(%d,%d)"
//	       ,vf1.at(0),vf1.at(1),vf2.at(0),vf2.at(1));

//	s3 = s1.operator*(s2);
	s3 = s1 * s2;
	cout<<"\n\tDoing * operator\n";
	s3.write_it( );

//	s3 = s1.operator-(s2);
	s3 = s1 - s2;
	cout<<"\n\tDoing - operator\n";
	s3.write_it( );

//	s3 = s1.operator-(s2);
	s3 = s1 + s2;
	cout<<"\n\tDoing + operator\n";
	s3.write_it( );

	return 0;
}
